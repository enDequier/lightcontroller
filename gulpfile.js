var gulp = require('gulp'),
    browserify = require('gulp-browserify'),
    stylus = require('gulp-stylus'),
    rename = require('gulp-rename'),
    paths = {
        javascript: './app/public/js/',
        javascriptSource: './app/resources/js/source/',
        css: './app/public/css',
        cssSource: './app/resources/css/'

    },
    names = {
        javascript: 'lc.compiled.js',
        javascriptSource: 'index.js',
        cssSource: 'style.styl'
    };


gulp.task('javascript', function() {
    gulp.src(paths.javascriptSource + names.javascriptSource)
        .pipe(browserify({
            insertGlobals : true,
            debug : !gulp.env.production
        }))
        .pipe(rename(names.javascript))
        .pipe(gulp.dest(paths.javascript));
});


gulp.task('css', function () {
    gulp.src(paths.cssSource + names.cssSource)
        .pipe(stylus())
        .pipe(gulp.dest(paths.css));
});

gulp.task('watch', function() {
    gulp.watch([paths.javascriptSource + '**/*.js'], ['javascript']);
    gulp.watch([paths.cssSource + '**/*.styl'], ['css']);
});