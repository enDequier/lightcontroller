var express = require('express'),
    app = express(),
    server = require('http').Server(app),
    io = require('socket.io').listen(server);

app.use(express.static(process.cwd() + '/app/public'));
app.get('/', function (request, response) {
    response.sendfile('./app/resources/views/layout.html');
});


require('./app/modules/Relays/relays.controller')(io);
require('./app/modules/DHT11/dht11.controller')(io);


server.listen(3000);