var Backbone = require('backbone'),
    DHT11Model = require("./dht11.model.js"),
    DHT11Collection = Backbone.Collection.extend({
        model: DHT11Model,

        initialize: function () {
            console.log('DHT11 sensors collection has been initialized');
            this._bindEvents();
        },

        _bindEvents: function () {
            this.on('add', function (model) {
                model.watch();
            });

            this.on('remove', function (model) {
                model.stopWatching();
            })
        }
    });

module.exports = DHT11Collection;