var DHT11Collection = require('./dht11.collection.js'),
    dht11Sensors = require('../../../configs/dht11.config.json'),
    _ = require('lodash'),
    sensors = new DHT11Collection();
    sensors.add(dht11Sensors);

var sendList = function (socket) {
    socket.emit('dht11_list', sensors.toJSON());
};

module.exports = function (io) {
    io.on('connection', function (socket) {
        socket.on('dht11_get_list', function () {
            sendList(socket);
        });

        sensors.on('change:value', function () {
            sendList(socket);
        })

    });
};

