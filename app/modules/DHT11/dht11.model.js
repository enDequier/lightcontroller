var Backbone = require('backbone'),
    sensorLib = require("node-dht-sensor"),
    _ = require('lodash'),
    DHT11Model = Backbone.Model.extend({
        defaults: {
            name: '',
            type: 11,
            value: {
                temperature: 0,
                humidity: 0
            },
            state: null,
            timeout: null,
            pin: null
        },

        toJSON: function () {
            return {
                value: this.get('value'),
                name: this.get('name')
            }
        },

        read: function () {
            var pin = this.get('pin');
            if (pin) {
                var sensorState = sensorLib.readSpec(this.get('type'), pin),
                    value = {
                        temperature: sensorState.temperature.toFixed(1) + '°C',
                        humidity: sensorState.humidity.toFixed(1) + '%'
                    };
                this.set('value', value);
                this.set('state', sensorState);
            }
        },

        watch: function () {
            this.read();
            this.timeout = setTimeout(_.bind(this.watch, this), 2000);
        },

        stopWatching: function () {
            if (this.timeout) {
                clearTimeout(this.timeout);
            }
        }
    });

module.exports = DHT11Model;