var RelaysCollection = require('./relays.collection.js'),
    relays = require('../../../configs/relays.config.json'),
    _ = require('lodash'),
    Relays = new RelaysCollection();

    Relays.add(relays);

var sendList = function (socket) {
    socket.emit('relays_list', Relays.toJSON());
};

module.exports = function (io) {
    io.on('connection', function (socket) {
        socket.on('relays_get_list', function () {
            sendList(socket);
        });

        socket.on('relays_toggle', function (id) {
            var relay = Relays.findWhere({id: id});
            if (relay) {
                relay.toggle().then(_.bind(sendList, null, io.sockets));
            }
        });
    });

};