var Backbone = require('backbone'),
    Promise = require('bluebird'),
    gpio = require('rpi-gpio'),

    ERRORS = {
        HAS_NOT_PIN: new Error('Пин не задан')
    },

    RelayModel = Backbone.Model.extend({
        defaults: {
            pin: undefined,
            name: 'Noname',
            value: 'none',
            id: undefined
        },

        hasPin: function () {
            return !!this.get('pin');
        },

        initialize: function () {
            this.updatePinValue();
        },

        updatePinValue: function () {
            var that = this;
            this._getPinValue().then(function (value) {
                that.set('value', value)
            })
        },

        toggle: function () {
            var that = this,
                pin = this.get('pin');
            return new Promise(function (resolve, reject) {
                gpio.setup(pin, gpio.DIR_OUT, function () {
                    var value = !that.get('value');
                    gpio.write(pin, value, function (err) {
                        if (err) {
                            reject(err)
                        } else {
                            that.set('value', value);
                            resolve(value);
                        }
                    });
                });
            })

        },

        _getPinValue: function () {
            var that = this;
            return new Promise(function (resolve, reject) {
                if (that.hasPin()) {
                    var pin = that.get('pin');
                    gpio.setup(pin, gpio.DIR_IN, function () {
                        gpio.read(pin, function (err, value) {
                            if (err) {
                                reject(err)
                            } else {
                                resolve(value);
                            }
                        })
                    });
                } else {
                    reject(ERRORS.HAS_NOT_PIN)
                }
            });
        }
    });

module.exports = RelayModel;