var Backbone = require('backbone'),
    RelayModel = require('./relay.model.js'),
    id = 1,

    RelaysCollection = Backbone.Collection.extend({
        model: RelayModel,
        initialize: function () {
            this._bindEvents();
            console.log('Relays collection has been initialized');
        },
        _bindEvents: function () {
            // auto increment id;
            this.on('add', function (model) {
                model.set('id', id);
                id += 1;
            })
        }
    });

module.exports = RelaysCollection;