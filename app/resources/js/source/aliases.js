var aliases = {
    //components
    'socket': './components/Socket/socket.js',
    'actions': './components/actions.js',
    'stores': './components/stores.js',
    'components': './components/components.js',

    //libs
    'socket.io': './libs/Socket-io/socket.io.js',

    //mixins
    'storeMixins': './mixins/reflux.store.mixins.js'
};

//configs for aliasify
module.exports = {
    aliases: aliases
};