var DHT11Model = require('./model.js'),
    Backbone = require('backbone'),
    DHT11Collection = Backbone.Collection.extend({
        model: DHT11Model
    });

module.exports = new DHT11Collection();