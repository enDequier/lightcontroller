var React = require('react'),
    _ = require('lodash');

var DHT11 = React.createClass({
    renderBody: function () {
        return (
            <div className="mc_content--body_wrapper">
                <span className="mc_content--body_text">{this.props.value}</span>
            </div>
        )
    },

    render: function() {
        return(
            <div className="col-md-3 col-xs-12">
                <div className="mc_content--block mc-relay-controller">
                    <div className="mc_content--header">
                        {this.props.name}
                    </div>
                    <div className="mc_content--body">
                        {this.renderBody()}
                        <div className="mc_content--loader_wrapper">
                            <div className="loader small green"></div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
});

module.exports = DHT11;

