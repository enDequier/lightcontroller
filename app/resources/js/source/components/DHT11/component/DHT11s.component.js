var React = require('react'),
    Reflux = require('reflux'),
    Stores = require('stores'),
    _ = require('lodash'),
    DHT11 = require('./DHT11.component.js'),

    _componentStore = Stores.dht11;

var DHT11s = React.createClass({
    mixins: [Reflux.listenTo(_componentStore, "setState")],
    getInitialState: _componentStore.getState,

    renderSensors: function () {
        var sensors = this.state.dht11,
            components = [];
        if (sensors) {
            sensors.each(function (sensor) {
                var value = sensor.get('value'),
                    name = sensor.get('name');
                components.push(<DHT11 name = {name + ': температура'} value = {value.temperature} />);
                components.push(<DHT11 name = {name + ': влажность'} value = {value.humidity} />);
            });
        }
        return components;
    },

    render: function() {
        return (
            <div className = "row">{this.renderSensors()}</div>
        )
    }
});

module.exports = DHT11s;
