var Actions = require('actions'),
    Reflux = require('reflux'),
    dht11Collection = require('./collection.js'),
    socket = require('socket');

var Store = Reflux.createStore({
    mixins: [require('storeMixins')],
    listenables: [Actions.dht11],
    state: {},
    init: function () {
        socket.emit('dht11_get_list');
    },
    dht11_list: function (list) {
        dht11Collection.set(list);
        this.state.dht11 = dht11Collection;
        this.update();
    }
});
require('./controller');

module.exports = Store;

