var actions = require('actions').dht11,
    socket = require('socket');

socket.on('dht11_list', function (list) {
    actions.dht11_list(list);
});
