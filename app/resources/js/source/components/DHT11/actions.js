var Reflux = require('reflux'),
    Actions = Reflux.createActions([
        'dht11_list'
    ]);

module.exports = Actions;