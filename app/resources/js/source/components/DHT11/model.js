var Backbone = require('backbone'),
    DHT11Model = Backbone.Model.extend({
        defaults: {
            name: '',
            value: ''
        }
    });

module.exports = DHT11Model;