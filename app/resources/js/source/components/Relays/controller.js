var actions = require('actions').relays,
    socket = require('socket');

socket.on('relays_list', function (list) {
    actions.relays_list(list);
});
