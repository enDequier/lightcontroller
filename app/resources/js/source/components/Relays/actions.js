var Reflux = require('reflux'),
    Actions = Reflux.createActions([
        'relay_toggle_clicked',
        'relays_list'
    ]);

module.exports = Actions;