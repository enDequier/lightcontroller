var Backbone = require('backbone'),
    RelaysCollection = Backbone.Collection.extend({
        model: require('./model.js')
    });

module.exports = new RelaysCollection();