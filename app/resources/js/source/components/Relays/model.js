var Backbone = require('backbone'),
    actions = require('actions').relays,
    RelayModel = Backbone.Model.extend({
        initialize: function () {
            this._bindEvents();
        },
        _bindEvents: function () {
            var that = this;
            this.on('change:value', function () {
                that.set('loading', false);
            });
        },
        toggle: function () {
            var id = this.get('id');
            this.set('loading', true);
            actions.relay_toggle_clicked(id);
        }
    });

module.exports = RelayModel;