var React = require('react'),
    _ = require('lodash');

var Relay = React.createClass({
    renderStatus: function () {
        var active = (this.props.relay.get('value')) ? '' : 'mc_content--status-active';
        return (
            <div className={'mc_content--status ' + active}></div>
        )
    },

    renderBody: function () {
        return (
            <div className="mc_content--body_wrapper">
                <button onClick = {this._toggle} className="mc_content--relay_switcher button">Переключить</button>
            </div>
        )
    },

    _toggle: function () {
        this.props.relay.toggle();
    },

    render: function() {
        return(
            <div className="col-md-3 col-xs-12">
                <div className="mc_content--block mc-relay-controller">
                    <div className="mc_content--header">
                        {this.props.relay.get('name')}
                        {this.renderStatus()}
                    </div>
                    <div className="mc_content--body">

                        {this.renderBody()}
                        <div className="mc_content--loader_wrapper">
                            <div className="loader small green"></div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
});

module.exports = Relay;

