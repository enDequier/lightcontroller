var React = require('react'),
    Reflux = require('reflux'),
    Actions = require('actions'),
    Stores = require('stores'),
    _ = require('lodash'),
    Relay = require('./Relay.component.js'),
    _componentStore = Stores.relays;

var Relays = React.createClass({
    mixins: [Reflux.listenTo(_componentStore, "setState")],
    getInitialState: _componentStore.getState,

    renderRelays: function () {
        var relays = this.state.relays,
            components = [];
        if (relays) {
            relays.each(function (relay) {
                components.push(<Relay key = {relay.get('id')} relay = {relay} />);
            });
        }
        return components;
    },

    render: function() {
        return (
            <div className = "row">{this.renderRelays()}</div>
        )
    }
});

module.exports = Relays;
