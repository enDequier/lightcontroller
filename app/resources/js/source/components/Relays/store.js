var Actions = require('actions'),
    Reflux = require('reflux'),
    relaysCollection = require('./collection.js'),
    socket = require('socket');

var Store = Reflux.createStore({
    mixins: [require('storeMixins')],
    listenables: [Actions.relays],
    state: {},
    init: function () {
        socket.emit('relays_get_list');
    },
    relay_toggle_clicked: function (id) {
        socket.emit('relays_toggle', id);
    },
    relays_list: function (list) {
        relaysCollection.set(list);
        this.state.relays = relaysCollection;
        this.update();
    }
});
require('./controller');

module.exports = Store;

