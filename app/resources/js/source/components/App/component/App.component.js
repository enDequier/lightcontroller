var React = require('react'),
    Reflux = require('reflux'),
    Stores = require('stores'),
    Components = require('components'),

    Blocks = [
        <Components.DHT11 />,
        <Components.Relays />
    ],

    App = React.createClass({
        render: function() {
            return  (
                <div className="wide-block">
                    <div className="inner">
                        <div className="mc_content container-fluid">
                            {Blocks}
                        </div>
                    </div>
                </div>
            )
        }
    });

React.render(<App />, document.getElementById('wrap'));