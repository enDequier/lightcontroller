module.exports = {
    getState: function () {
        return this.state;
    },
    update: function () {
        this.trigger(this.state);
    }
};